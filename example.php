<?php

define(
    'SEOWORK_PATH',
    realpath(__DIR__ . DIRECTORY_SEPARATOR . 'lib')
); // По умолчанию не менять. Рядом с файлом example должна быть папка lib.  Если папка другая, то нужно изменить константу __DIR__

error_reporting(E_ALL);

ini_set('display_errors', 1);

$domain = 'example.ru'; // название домена без http:// — используется для создания директории, поэтому не нужно использовать спец символов

require_once SEOWORK_PATH . '/Seowork.php';
$banner = new Seowork(
    [
        'bot.header'             => 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)',
        //bot.comparison.mode: 1 - точное сравнение, 0 - вхождение
        'bot.comparison.mode'    => 1,
        'app.name'               => $domain,
        'app.path'               => SEOWORK_PATH,
        'app.limit'              => 1,
        // количество ссылок на страницу («лимит ссылок на страницу» в настройках проекта)
        'app.status'             => 1,
        'app.charset'            => 'UTF-8',
        // кодировка сайта — «windows-1251» или «UTF-8»
        'app.no_index'           => 1,
        // если «1» — на главной странице не размещаем ссылки, если «0» — размещаем
        'app.token'              => 'TOKEN',
        // токен площадки
        'app.token.dir'          => 'token',
        'app.print'              => 1,
        // «1» ссылки выводим сразу на странице, где вызывается скрип. Если «0», то мы сохраняем текст ссылок в переменную, например «$output = $webit->run();»
        'api.req.timeout.new'    => 3,
        // Как часто мы будем делать запросы на размещение (в секундах)
        'api.req.timeout.delete' => 300,
        // Как часто мы будем делать запросы на удаление (в секундах)
        'api.req.timeout.dir'    => 'timeout',
        // Храним информацию о таймаутах, когда делали запрос.
        'api.res.timeout'        => 3,
        // Время ожидания ответа от сервера (в секундах)
        'storage.driver'         => 'sqlite',
        //'storage.driver'            => 'pdo',    // Если используется MySQL, то не нужно изменять. Если используется другой драйвер БД, то нужно обратиться в поддержку
        //'storage.dsn'               => 'mysql:host='.$database['host'].';port=3306;dbname='.$database['name'],
        // Если используется MySQL, то не нужно изменять. Если используется другой драйвер БД, то нужно обратиться в поддержку
        'storage.dsn'            => 'sqlite:' . realpath(__DIR__ . DIRECTORY_SEPARATOR . 'database/database.sqlite'),
        'storage.charset'        => 'UTF-8',
        'storage.table'          => 'Web_IT_Url',
        'storage.options'        => [
            PDO::ATTR_ERRMODE          => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_AUTOCOMMIT       => false,
            PDO::ATTR_EMULATE_PREPARES => 1,
        ],
        'debug.mode'             => 1,
        'debug.token'            => 'DEBUG_TOKEN',
        // токен площадки для дебага
        
        //'template.css'
        //'template.master'
        //'template.slave'
        //'template.footer'
    ]
);

$banner->run();