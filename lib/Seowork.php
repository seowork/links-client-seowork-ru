<?php

/**
 * Class Seowork
 *
 * Для обратной совместимости с версиями PHP < 5.4 НЕОБХОДИМО использовать
 * старый синтаксис объявления массивов array()
 */

class Seowork
{
    const DEFAULT_CHARSET = 'UTF-8';
    const URL_NEW         = 'https://ns.seowork.ru/url/packet/api/v2/new';
    const URL_DELETE      = 'https://ns.seowork.ru/url/packet/api/v2/delete';
    const TYPE_NEW        = 'new';
    const TYPE_DELETE     = 'delete';
    const DENY_TYPE       = '#ico|css|js|jpg|jpeg|png#i';
    const TEMPLATE_MASTER = '<div class="block block_grey" data-partnership><div class="block__body" data-partnership>%s</div>%s</div>';
    const TEMPLATE_SLAVE  = '<p data-partnership>%s</p>';
    const TEMPLATE_FOOTER = '<div class="block__footer" data-partnership><a class="block__logo" data-partnership href="#">Реклама</a></div>';
    const TEMPLATE_CSS    = '[data-partnership]{box-sizing:border-box;margin:0;padding:0;font-family:Arial}.block[data-partnership]{width:100%;max-width: 320px;display:block}.block.block_grey[data-partnership] .block__body[data-partnership]{border-color:#979797}.block.block_grey[data-partnership] .block__footer[data-partnership]{background-color:#979797}.block.block_red[data-partnership] .block__body[data-partnership]{border-color:#F2412F}.block.block_red[data-partnership] .block__footer[data-partnership]{background-color:#F2412F}.block__body[data-partnership]{display:block;padding:20px;min-height:110px;border-width:1px;border-style:solid;width:100%}.block__body[data-partnership] p[data-partnership]{color:#212121;font-size:14px;line-height:20px;margin-bottom:5px}.block__body[data-partnership] p[data-partnership]:last-of-type{margin-bottom:0}.block__body[data-partnership] a[data-partnership]{color:#2B7BE1;text-decoration:none;transition:.3s}.block__body[data-partnership] a[data-partnership]:hover{color:#255197}.block__footer[data-partnership]{display:flex;align-items:center;width:100%;height:56px;padding:8px 24px;overflow:hidden}.block__logo[data-partnership]{display:block;max-height:40px;text-decoration:none;color:#FFF;font-size:18px;line-height:20px;overflow:hidden}.block__footer[data-partnership] img[data-partnership],.block__footer[data-partnership] svg[data-partnership],.block__logo[data-partnership] img[data-partnership]{max-height:40px;max-width:270px}';
    protected $error;
    protected $log = array();
    /**
     * @var Seowork_ParameterBag
     */
    public $cfg;
    /**
     * @var Seowork_Http_Request
     */
    public $req;
    /**
     * @var Seowork_Filesystem
     */
    protected $fs;
    protected $out;
    /**
     * @var Seowork_Api_Interface
     */
    protected $api;
    /**
     * @var Seowork_Storage_PDO_MySQL
     */
    protected $storage;
    /**
     * @var Seowork_Api_TimeOut
     */
    protected $timeout;
    
    public function __construct(array $config = array())
    {
        $this->fs  = new Seowork_Filesystem;
        $this->req = new Seowork_Http_Request;
        
        $this->setConfig($config);
        
        Seowork_Debugger::debug($this);
    }
    
    public function setConfig(array $config = array())
    {
        $this->cfg = new Seowork_ParameterBag($config);
        
        if ($this->cfg->get('debug.mode')
            && $this->cfg->has('debug.token')
            && $this->req->headers->has(Seowork_Debugger::DEBUG_HEADER)
            && ($this->req->headers->get(Seowork_Debugger::DEBUG_HEADER) === $this->cfg->get('debug.token'))
        ) {
            Seowork_Debugger::enableDebugMode();
        }
        
        if (!$this->cfg->has('template.css')) {
            $this->cfg->set('template.css', self::TEMPLATE_CSS);
        }
        
        if (!$this->cfg->has('template.footer')) {
            $this->cfg->set('template.footer', self::TEMPLATE_FOOTER);
        }
        
        if (!$this->cfg->has('template.master')) {
            $this->cfg->set('template.master', self::TEMPLATE_MASTER);
        }
        
        if (!$this->cfg->has('template.slave')) {
            $this->cfg->set('template.slave', self::TEMPLATE_SLAVE);
        }
        
        if (!$this->cfg->has('app.token')) {
            $this->cfg->set('app.token', $this->getToken());
        }
        
        if (!$this->cfg->has('bot.comparison.mode')) {
            $this->cfg->set('bot.comparison.mode', 1);
        }
        
        if (!$this->cfg->has('app.token') || false === $this->cfg->get('app.token')) {
            $this->cfg->set('app.status', 0);
            
            $this->error = 'token error';
            
            return false;
        }
        
        if ($this->cfg->has('app.no_index') && $this->cfg->get('app.no_index') && $this->req->isUriIndex()) {
            $this->cfg->set('app.status', 0);
            
            $this->error = 'index page';
            
            return false;
        }
        
        if (preg_match(self::DENY_TYPE, $this->req->getPathInfo())) {
            $this->cfg->set('app.status', 0);
            
            $this->error = sprintf('Type error "%s"', $this->req->getPathInfo());
            
            return false;
        }
        
        $requestUserAgent = $this->req->headers->get('user-agent');
        $validUserAgent = $this->cfg->get('bot.header');
        $comparisonMode = $this->cfg->get('bot.comparison.mode');
        
        $isBotMode = 1 === $comparisonMode
            ? $requestUserAgent === $validUserAgent
            : false !== strpos($requestUserAgent, $validUserAgent);
        
        $this->cfg->set('bot.status', (int) $isBotMode);
        
        $this->cfg->set('uri.hash', $this->req->getRealUriMd5Hash());
        $this->cfg->set(
            'app.charset_status',
            (int) (self::DEFAULT_CHARSET === strtoupper($this->cfg->get('app.charset')))
        );
        
        if ($this->cfg->get('app.status')) {
            if (!$this->cfg->has('api.req.timeout.dir')) {
                $this->cfg->set('api.req.timeout.dir', 'timeout');
            }
            
            if (!$this->cfg->has('api.req.timeout.new')) {
                $this->cfg->set('api.req.timeout.new', 10);
            }
            
            if (!$this->cfg->has('api.req.timeout.delete')) {
                $this->cfg->set('api.req.timeout.delete', 60);
            }
            
            if (!$this->cfg->has('api.res.timeout')) {
                $this->cfg->set('api.res.timeout', 5);
            }
            
            if (!$this->cfg->has('storage.driver')) {
                $this->cfg->set('storage.driver', 'file');
                $this->cfg->set('storage.dir', 'cache');
            }
            
            if ('file' === $this->cfg->get('storage.driver')) {
                $this->cfg->set('storage.path', $this->getStoragePath());
                
                if (!$this->fs->isWritable($this->cfg->get('storage.path'))) {
                    $this->cfg->set('app.status', 0);
                    
                    $this->error = sprintf(
                        'driver: %s, path: %s is not writable',
                        $this->cfg->get('storage.driver'),
                        $this->cfg->get('storage.path')
                    );
                    
                    return false;
                }
                
                $this->cfg->set('storage.path.status', 1);
            } elseif ('sqlite' === $this->cfg->get('storage.driver')) {
                $this->storage = new Seowork_Storage_PDO_SQLite(
                    $this->cfg->get('uri.hash'),
                    $this->req->getRealUri(),
                    $this->cfg->get('storage.dsn'),
                    $this->cfg->get('storage.options')
                );
                
                if (!$this->storage->getStatus()) {
                    $this->cfg->set('app.status', 0);
                    
                    $this->error = 'PDO status error';
                    
                    return false;
                }
            } elseif ('pdo' === $this->cfg->get('storage.driver')) {
                $this->storage = new Seowork_Storage_PDO_MySQL(
                    $this->cfg->get('storage.table'),
                    $this->cfg->get('uri.hash'),
                    $this->req->getRealUri(),
                    $this->cfg->get('storage.dsn'),
                    $this->cfg->get('storage.user'),
                    $this->cfg->get('storage.password'),
                    $this->cfg->get('storage.options')
                );
                
                if (!$this->storage->getStatus()) {
                    $this->cfg->set('app.status', 0);
                    
                    $this->error = 'PDO status error';
                    
                    return false;
                }
            }
            
            $this->cfg->set('api.req.timeout.path', $this->getTimeOutPath());
            
            if (!$this->fs->isWritable($this->cfg->get('api.req.timeout.path'))) {
                $this->cfg->set('app.status', 0);
                
                $this->error = sprintf(
                    'timeout dir "%s" is not writable',
                    $this->cfg->get('api.req.timeout.path')
                );
                
                return false;
            }
            
            $this->cfg->set('api.req.timeout.path.status', 1);
            
            $this->timeout = new Seowork_Api_TimeOut(
                array(
                    'path' => $this->cfg->get('api.req.timeout.path'),
                    'name' => $this->cfg->get('app.name'),
                )
            );
            
            $this->api = Seowork_Api_Api::getAgent(
                array(
                    'hash'      => $this->cfg->get('uri.hash'),
                    'token'     => $this->cfg->get('app.token'),
                    'timeout'   => $this->cfg->get('api.res.timeout'),
                    'referer'   => $this->req->getRealUri(),
                    'userAgent' => $this->req->headers->get('user-agent'),
                )
            );
        }
        
        return $this;
    }
    
    public function getToken()
    {
        $path = $this->cfg->get('app.path')
            . DIRECTORY_SEPARATOR . $this->cfg->get('app.token.dir', 'token')
            . DIRECTORY_SEPARATOR . $this->cfg->get('app.name');
        
        return file_get_contents($path);
    }
    
    public function getTimeOutPath()
    {
        return $this->cfg->get('app.path') . DIRECTORY_SEPARATOR
            . $this->cfg->get('api.req.timeout.dir');
    }
    
    public function getStoragePath()
    {
        return $this->cfg->get('app.path') . DIRECTORY_SEPARATOR
            . $this->cfg->get('storage.dir');
    }
    
    private function toCharset($array)
    {
        if (!$this->cfg->get('app.charset_status')) {
            $charset = $this->cfg->get('app.charset');
            
            foreach ($array as $k => $v) {
                $array[$k] = iconv('UTF-8', $charset, $v);
            }
        }
        
        return $array;
    }
    
    private function toString($array)
    {
        $links         = array();
        $templateSlave = $this->cfg->get('template.slave');
        
        foreach (array_values($this->toCharset($array)) as $value) {
            $links[] = sprintf($templateSlave, str_replace('<a ', '<a data-partnership ', $value));
        }
        
        return sprintf(
            '<style type="text/css">%s</style>%s',
            $this->cfg->get('template.css'),
            sprintf(
                $this->cfg->get('template.master'),
                implode('', $links),
                $this->cfg->get('template.footer')
            )
        );
    }
    
    public function run()
    {
        if (!$this->cfg->get('app.status')) {
            return false;
        }
        
        if ($this->cfg->get('bot.status')) {
            $data = $this->storage->findAllByHash($this->cfg->get('uri.hash'));
            Seowork_Debugger::debug($data);
            $this->log['db_data'] = $data;
            if (null !== $data) {
                if (count($data) < $this->cfg->get('app.limit')
                    && $this->timeout->diff(self::TYPE_NEW) > $this->cfg->get('api.req.timeout.new')
                ) {
                    $this->timeout->put(self::TYPE_NEW);
                    $response = $this->api->send(self::URL_NEW);
                    Seowork_Debugger::debug($response);
                    $this->log['req_response_new'] = $response;
                    
                    if (null !== $response) {
                        $data2 = $this->storage->save($response);
                        Seowork_Debugger::debug($data2);
                        if ($data2) {
                            $data = array_merge($data, $data2);
                        }
                    }
                }
                
                $this->out = $this->toString($data);
            } elseif ($this->timeout->diff(self::TYPE_NEW) > $this->cfg->get('api.req.timeout.new')) {
                $this->timeout->put(self::TYPE_NEW);
                $response = $this->api->send(self::URL_NEW);
                Seowork_Debugger::debug($response);
                $this->log['req_response_new'] = $response;
                
                if (null !== $response) {
                    $data = $this->storage->save($response);
                    Seowork_Debugger::debug($data);
                    $this->log['req_data'] = $data;
                    if ($data) {
                        $this->out = $this->toString($data);
                    }
                }
            }
        } else {
            $data = $this->storage->findAllByHash($this->cfg->get('uri.hash'));
            Seowork_Debugger::debug($data);
            $this->log['db_data'] = $data;
            if (null !== $data) {
                $this->out = $this->toString($data);
            }
        }
        
        if ($this->timeout->diff(self::TYPE_DELETE) > $this->cfg->get('api.req.timeout.delete')) {
            $this->timeout->put(self::TYPE_DELETE);
            
            $response = $this->api->send(self::URL_DELETE);
            Seowork_Debugger::debug($response);
            $this->log['req_response_del'] = $response;
            
            if (null !== $response) {
                $this->storage->delete($response);
            }
        }
        
        Seowork_Debugger::debug($this->out);
        
        $this->log['out'] = $this->out;
        if (!$this->cfg->get('app.print')) {
            return $this->out;
        } else {
            echo $this->out;
        }
        
        return null;
    }
}

interface Seowork_Api_Interface
{
    public function send($url);
}

abstract class Seowork_Api_Api implements Seowork_Api_Interface
{
    /**
     * @var string
     */
    protected $hash;
    /**
     * @var string
     */
    protected $token;
    /**
     * @var string
     */
    protected $referer;
    /**
     * @var float|int
     */
    protected $timeout;
    /**
     * @var string
     */
    protected $userAgent;
    /**
     * @var array
     */
    protected $log = array();
    
    public function __construct(array $config = array())
    {
        foreach ($config as $k => $v) {
            $this->{$k} = $v;
        }
    }
    
    public static function getAgent($config)
    {
        if (!function_exists('curl_init')) {
            return new Seowork_Api_FileContent($config);
        }
        
        return new Seowork_Api_Curl($config);
    }
}

class Seowork_Api_Curl extends Seowork_Api_Api
{
    /**
     * @var resource
     */
    private $curl;
    
    /**
     * Seowork_Api_Api constructor.
     *
     * @param array $config
     */
    public function __construct(array $config = array())
    {
        parent::__construct($config);
        $this->curl = curl_init();
        
        curl_setopt($this->curl, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($this->curl, CURLOPT_CONNECTTIMEOUT, $this->timeout);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_POST, 1);
        curl_setopt($this->curl, CURLOPT_USERAGENT, $this->userAgent);
        curl_setopt($this->curl, CURLOPT_REFERER, $this->referer);
        curl_setopt(
            $this->curl,
            CURLOPT_POSTFIELDS,
            array(
                'token' => $this->token,
                'hash'  => $this->hash,
            )
        );
    }
    
    /**
     * @param $url
     *
     * @return null|array
     */
    public function send($url)
    {
        curl_setopt($this->curl, CURLOPT_URL, $url);
        
        $data = curl_exec($this->curl);
        
        $this->log['url']   = $url;
        $this->log['data']  = $data;
        $this->log['error'] = array(
            'errno' => curl_errno($this->curl),
            'error' => curl_error($this->curl),
        );
        $this->log['info']  = $this->info();
        
        Seowork_Debugger::debug($data, $this->info(), curl_errno($this->curl), curl_error($this->curl));
        
        if (0 === curl_errno($this->curl) && 200 === $this->info(CURLINFO_HTTP_CODE)) {
            $data = json_decode($data, 1);
            
            return $data['data'];
        }
        
        return null;
    }
    
    /**
     * @param null $param
     *
     * @return mixed
     */
    public function info($param = null)
    {
        return $param
            ? curl_getinfo($this->curl, $param)
            : curl_getinfo($this->curl);
    }
    
    /**
     * @return bool
     */
    public function close()
    {
        curl_close($this->curl);
        
        return true;
    }
}

class Seowork_Api_FileContent extends Seowork_Api_Api
{
    /**
     * @var resource
     */
    private $_context;
    /**
     * @var array
     */
    private $_options = array();
    
    public function __construct(array $config)
    {
        parent::__construct($config);
        
        $this->_options = array(
            'http' => array(
                'method'     => 'POST',
                'user_agent' => $this->userAgent,
                'timeout'    => $this->timeout,
                'header'     => sprintf(
                    "Referer: %s\r\n%s",
                    $this->referer,
                    "Content-type: application/x-www-form-urlencoded\r\n"
                ),
                'content'    => http_build_query(
                    array(
                        'token' => $this->token,
                        'hash'  => $this->hash,
                    )
                ),
            ),
        );
        $this->_context = stream_context_create($this->_options);
    }
    
    /**
     * @param $url
     *
     * @return array|null
     */
    public function send($url)
    {
        $data = @file_get_contents($url, null, $this->_context);
        $info = $http_response_header;
        preg_match('/HTTP\/[\d\.]+\s+(?P<code>[\d]+)/', $info[0], $matches);
        
        Seowork_Debugger::debug($data, $info);
        
        if ($data !== false && (int) $matches['code'] === 200) {
            $data = json_decode($data, 1);
            
            return $data['data'];
        }
        
        return null;
    }
}

class Seowork_Api_TimeOut extends Seowork_Filesystem
{
    protected $path;
    protected $name;
    
    /**
     * Seowork_Api_TimeOut constructor.
     *
     * @param array $config
     */
    public function __construct(array $config = array())
    {
        foreach ($config as $k => $v) {
            $this->{$k} = $v;
        }
    }
    
    /**
     * @param $type
     *
     * @return string
     */
    public function getFileName($type)
    {
        return $this->path . DIRECTORY_SEPARATOR . $this->name . '_' . $type;
    }
    
    /**
     * @param $type
     *
     * @return bool
     */
    public function exists($type)
    {
        return file_exists($this->getFileName($type));
    }
    
    /**
     * @param $type
     *
     * @return int
     */
    public function put($type)
    {
        return file_put_contents($this->getFileName($type), time());
    }
    
    /**
     * @param $type
     *
     * @return bool|int
     */
    public function get($type)
    {
        return $this->exists($type)
            ? (int) file_get_contents($this->getFileName($type))
            : false;
    }
    
    /**
     * @param $type
     *
     * @return int
     */
    public function diff($type)
    {
        return $this->exists($type) ? time() - $this->get($type) : 100500;
    }
}

class Seowork_Filesystem
{
    /**
     * @param     $path
     * @param int $mode
     *
     * @return bool
     * @throws Exception
     */
    public function mkdir($path, $mode = 0777)
    {
        if (is_dir($path)) {
            return true;
        }
        
        if (!mkdir($path, $mode, true) && !is_dir($path)) {
            throw new RuntimeException(sprintf('Failed to create "%s".', $path));
        }
        
        return true;
    }
    
    /**
     * @param $path
     *
     * @return bool
     */
    public function exists($path)
    {
        return file_exists($path);
    }
    
    /**
     * @param $path
     *
     * @return bool
     */
    public function isWritable($path)
    {
        return $this->exists($path) && is_writable($path);
    }
    
    /**
     * @param $path
     *
     * @return bool
     */
    public function remove($path)
    {
        return $this->exists($path) && unlink($path);
    }
    
    /**
     * @param     $path
     * @param     $mode
     * @param int $umask
     *
     * @return bool
     */
    public function chmod($path, $mode, $umask = 0000)
    {
        return $this->exists($path) && chmod($path, $mode & ~$umask);
    }
}

class Seowork_Http_HeaderBag implements IteratorAggregate, Countable
{
    protected $headers      = array();
    protected $cacheControl = array();
    
    /**
     * Constructor.
     *
     * @param array $headers An array of HTTP headers
     *
     * @api
     */
    public function __construct(array $headers = array())
    {
        foreach ($headers as $key => $values) {
            $this->set($key, $values);
        }
    }
    
    /**
     * Returns the headers as a string.
     *
     * @return string The headers
     */
    public function __toString()
    {
        if (!$this->headers) {
            return '';
        }
        
        $max     = max(array_map('strlen', array_keys($this->headers))) + 1;
        $content = '';
        ksort($this->headers);
        foreach ($this->headers as $name => $values) {
            $name = implode('-', array_map('ucfirst', explode('-', $name)));
            foreach ($values as $value) {
                $content .= sprintf("%-{$max}s %s\r\n", $name . ':', $value);
            }
        }
        
        return $content;
    }
    
    /**
     * Returns the headers.
     *
     * @return array An array of headers
     *
     * @api
     */
    public function all()
    {
        return $this->headers;
    }
    
    /**
     * Returns the parameter keys.
     *
     * @return array An array of parameter keys
     *
     * @api
     */
    public function keys()
    {
        return array_keys($this->headers);
    }
    
    /**
     * Replaces the current HTTP headers by a new set.
     *
     * @param array $headers An array of HTTP headers
     *
     * @api
     */
    public function replace(array $headers = array())
    {
        $this->headers = array();
        $this->add($headers);
    }
    
    /**
     * Adds new headers the current HTTP headers set.
     *
     * @param array $headers An array of HTTP headers
     *
     * @api
     */
    public function add(array $headers)
    {
        foreach ($headers as $key => $values) {
            $this->set($key, $values);
        }
    }
    
    /**
     * Returns a header value by name.
     *
     * @param string $key     The header name
     * @param mixed  $default The default value
     * @param bool   $first   Whether to return the first value or all header values
     *
     * @return string|array The first header value if $first is true, an array of values otherwise
     *
     * @api
     */
    public function get($key, $default = null, $first = true)
    {
        $key = strtr(strtolower($key), '_', '-');
        
        if (!array_key_exists($key, $this->headers)) {
            if (null === $default) {
                return $first ? null : array();
            }
            
            return $first ? $default : array($default);
        }
        
        if ($first) {
            return count($this->headers[$key]) ? $this->headers[$key][0] : $default;
        }
        
        return $this->headers[$key];
    }
    
    /**
     * Sets a header by name.
     *
     * @param string       $key     The key
     * @param string|array $values  The value or an array of values
     * @param bool         $replace Whether to replace the actual value or not (true by default)
     *
     * @api
     */
    public function set($key, $values, $replace = true)
    {
        $key = strtr(strtolower($key), '_', '-');
        
        $values = array_values((array) $values);
        
        if (true === $replace || !isset($this->headers[$key])) {
            $this->headers[$key] = $values;
        } else {
            $this->headers[$key] = array_merge($this->headers[$key], $values);
        }
        
        if ('cache-control' === $key) {
            $this->cacheControl = $this->parseCacheControl($values[0]);
        }
    }
    
    /**
     * Returns true if the HTTP header is defined.
     *
     * @param string $key The HTTP header
     *
     * @return bool    true if the parameter exists, false otherwise
     *
     * @api
     */
    public function has($key)
    {
        return array_key_exists(strtr(strtolower($key), '_', '-'), $this->headers);
    }
    
    /**
     * Returns true if the given HTTP header contains the given value.
     *
     * @param string $key   The HTTP header name
     * @param string $value The HTTP value
     *
     * @return bool    true if the value is contained in the header, false otherwise
     *
     * @api
     */
    public function contains($key, $value)
    {
        return in_array($value, $this->get($key, null, false));
    }
    
    /**
     * Removes a header.
     *
     * @param string $key The HTTP header name
     *
     * @api
     */
    public function remove($key)
    {
        $key = strtr(strtolower($key), '_', '-');
        
        unset($this->headers[$key]);
        
        if ('cache-control' === $key) {
            $this->cacheControl = array();
        }
    }
    
    /**
     * Returns the HTTP header value converted to a date.
     *
     * @param string    $key     The parameter key
     * @param \DateTime $default The default value
     *
     * @return null|\DateTime The parsed DateTime or the default value if the header does not exist
     *
     * @throws RuntimeException When the HTTP header is not parseable
     *
     * @api
     */
    public function getDate($key, DateTime $default = null)
    {
        if (null === $value = $this->get($key)) {
            return $default;
        }
        
        if (false === $date = DateTime::createFromFormat(DATE_RFC2822, $value)) {
            throw new RuntimeException(sprintf('The %s HTTP header is not parseable (%s).', $key, $value));
        }
        
        return $date;
    }
    
    /**
     * Adds a custom Cache-Control directive.
     *
     * @param string $key   The Cache-Control directive name
     * @param mixed  $value The Cache-Control directive value
     */
    public function addCacheControlDirective($key, $value = true)
    {
        $this->cacheControl[$key] = $value;
        
        $this->set('Cache-Control', $this->getCacheControlHeader());
    }
    
    /**
     * Returns true if the Cache-Control directive is defined.
     *
     * @param string $key The Cache-Control directive
     *
     * @return bool true if the directive exists, false otherwise
     */
    public function hasCacheControlDirective($key)
    {
        return array_key_exists($key, $this->cacheControl);
    }
    
    /**
     * Returns a Cache-Control directive value by name.
     *
     * @param string $key The directive name
     *
     * @return mixed|null The directive value if defined, null otherwise
     */
    public function getCacheControlDirective($key)
    {
        return array_key_exists($key, $this->cacheControl) ? $this->cacheControl[$key] : null;
    }
    
    /**
     * Removes a Cache-Control directive.
     *
     * @param string $key The Cache-Control directive
     */
    public function removeCacheControlDirective($key)
    {
        unset($this->cacheControl[$key]);
        
        $this->set('Cache-Control', $this->getCacheControlHeader());
    }
    
    /**
     * Returns an iterator for headers.
     *
     * @return \ArrayIterator An \ArrayIterator instance
     */
    public function getIterator()
    {
        return new ArrayIterator($this->headers);
    }
    
    /**
     * Returns the number of headers.
     *
     * @return int The number of headers
     */
    public function count()
    {
        return count($this->headers);
    }
    
    protected function getCacheControlHeader()
    {
        $parts = array();
        ksort($this->cacheControl);
        foreach ($this->cacheControl as $key => $value) {
            if (true === $value) {
                $parts[] = $key;
            } else {
                if (preg_match('#[^a-zA-Z0-9._-]#', $value)) {
                    $value = '"' . $value . '"';
                }
                
                $parts[] = "$key=$value";
            }
        }
        
        return implode(', ', $parts);
    }
    
    /**
     * Parses a Cache-Control HTTP header.
     *
     * @param string $header The value of the Cache-Control HTTP header
     *
     * @return array An array representing the attribute values
     */
    protected function parseCacheControl($header)
    {
        $cacheControl = array();
        preg_match_all('#([a-zA-Z][a-zA-Z_-]*)\s*(?:=(?:"([^"]*)"|([^ \t",;]*)))?#', $header, $matches, PREG_SET_ORDER);
        foreach ($matches as $match) {
            $cacheControl[strtolower($match[1])] = isset($match[3]) ? $match[3] : (isset($match[2]) ? $match[2] : true);
        }
        
        return $cacheControl;
    }
}

class Seowork_Http_Request
{
    /**
     * @var \Seowork_Http_ServerBag
     */
    public $server;
    /**
     * @var \Seowork_Http_HeaderBag
     */
    public    $headers;
    protected $method;
    protected $baseUrl;
    protected $basePath;
    protected $pathInfo;
    protected $requestUri;
    
    /**
     * Seowork_Http_Request constructor.
     */
    public function __construct()
    {
        $this->server  = new Seowork_Http_ServerBag($_SERVER);
        $this->headers = new Seowork_Http_HeaderBag($this->server->getHeaders());
        
        Seowork_Debugger::debug($this);
    }
    
    public function isMethod($method)
    {
        return $this->getMethod() === strtoupper($method);
    }
    
    public function isMethodSafe()
    {
        return in_array(
            $this->getMethod(),
            array(
                'GET',
                'HEAD',
            )
        );
    }
    
    public function getMethod()
    {
        if (null === $this->method) {
            $this->method = strtoupper($this->server->get('REQUEST_METHOD', 'GET'));
            
            if ('POST' === $this->method) {
                if ($method = $this->headers->get('X-HTTP-METHOD-OVERRIDE')) {
                    $this->method = strtoupper($method);
                }
            }
        }
        
        return $this->method;
    }
    
    public function getScriptName()
    {
        return $this->server->get('SCRIPT_NAME', $this->server->get('ORIG_SCRIPT_NAME', ''));
    }
    
    public function getScheme()
    {
        return $this->isSecure() ? 'https' : 'http';
    }
    
    public function getPort()
    {
        if ($host = $this->headers->get('HOST')) {
            if (false !== $pos = strrpos($host, ':')) {
                return (int) substr($host, $pos + 1);
            }
            
            return 'https' === $this->getScheme() ? 443 : 80;
        }
        
        return $this->server->get('SERVER_PORT');
    }
    
    public function isSecure()
    {
        return 'on' == strtolower($this->server->get('HTTPS')) || 1 == $this->server->get('HTTPS');
    }
    
    public function getHost()
    {
        if (!$host = $this->headers->get('HOST')) {
            if (!$host = $this->server->get('SERVER_NAME')) {
                $host = $this->server->get('SERVER_ADDR', '');
            }
        }
        
        // trim and remove port number from host
        // host is lowercase as per RFC 952/2181
        $host = strtolower(preg_replace('/:\d+$/', '', trim($host)));
        
        // as the host can come from the user (HTTP_HOST and depending on the configuration, SERVER_NAME too can come from the user)
        // check that it does not contain forbidden characters (see RFC 952 and RFC 2181)
        if ($host && !preg_match('/^\[?(?:[a-zA-Z0-9-:\]_]+\.?)+$/', $host)) {
            throw new UnexpectedValueException(sprintf('Invalid Host "%s"', $host));
        }
        
        return $host;
    }
    
    public function getHttpHost()
    {
        $scheme = $this->getScheme();
        $port   = $this->getPort();
        
        if (('http' == $scheme && $port == 80) || ('https' == $scheme && $port == 443)) {
            return $this->getHost();
        }
        
        return $this->getHost() . ':' . $port;
    }
    
    public function isNoCache()
    {
        return $this->headers->hasCacheControlDirective('no-cache') || 'no-cache' == $this->headers->get('Pragma');
    }
    
    public function getPathInfo()
    {
        if (null === $this->pathInfo) {
            $this->pathInfo = $this->preparePathInfo();
        }
        
        return $this->pathInfo;
    }
    
    public function getBaseUrl()
    {
        if (null === $this->baseUrl) {
            $this->baseUrl = $this->prepareBaseUrl();
        }
        
        return $this->baseUrl;
    }
    
    public function isUriIndex()
    {
        return '/' === $this->getRequestUri();
    }
    
    public function getRequestUri()
    {
        if (null === $this->requestUri) {
            $this->requestUri = $this->prepareRequestUri();
        }
        
        return $this->requestUri;
    }
    
    public function getSchemeAndHttpHost()
    {
        return $this->getScheme() . '://' . $this->getHttpHost();
    }
    
    public function getRealMethod()
    {
        return strtoupper($this->server->get('REQUEST_METHOD', 'GET'));
    }
    
    public function getQueryString()
    {
        $qs = self::normalizeQueryString($this->server->get('QUERY_STRING'));
        
        return '' === $qs ? null : $qs;
    }
    
    public function getBasePath()
    {
        if (null === $this->basePath) {
            $this->basePath = $this->prepareBasePath();
        }
        
        return $this->basePath;
    }
    
    public function getRealUri()
    {
        return $this->getSchemeAndHttpHost() . $this->getRequestUri();
    }
    
    public function getRealUriMd5Hash()
    {
        $uri = strtolower(
            str_replace(
                array(
                    'http://',
                    'https://',
                    'www.',
                    ':80',
                ),
                '',
                rtrim($this->getRealUri(), '/')
            )
        );
        
        return md5($uri);
    }
    
    public function getUri()
    {
        if (null !== $qs = $this->getQueryString()) {
            $qs = '?' . $qs;
        }
        
        return $this->getSchemeAndHttpHost() . $this->getBaseUrl() . $this->getPathInfo() . $qs;
    }
    
    public function getUriForPath($path)
    {
        return $this->getSchemeAndHttpHost() . $this->getBaseUrl() . $path;
    }
    
    protected function preparePathInfo()
    {
        $baseUrl = $this->getBaseUrl();
        
        if (null === ($requestUri = $this->getRequestUri())) {
            return '/';
        }
        
        $pathInfo = '/';
        
        // Remove the query string from REQUEST_URI
        if ($pos = strpos($requestUri, '?')) {
            $requestUri = substr($requestUri, 0, $pos);
        }
        
        if (null !== $baseUrl && false === $pathInfo = substr($requestUri, strlen($baseUrl))) {
            // If substr() returns false then PATH_INFO is set to an empty string
            return '/';
        } elseif (null === $baseUrl) {
            return $requestUri;
        }
        
        return (string) $pathInfo;
    }
    
    protected function prepareBaseUrl()
    {
        $filename = basename($this->server->get('SCRIPT_FILENAME'));
        
        if (basename($this->server->get('SCRIPT_NAME')) === $filename) {
            $baseUrl = $this->server->get('SCRIPT_NAME');
        } elseif (basename($this->server->get('PHP_SELF')) === $filename) {
            $baseUrl = $this->server->get('PHP_SELF');
        } elseif (basename($this->server->get('ORIG_SCRIPT_NAME')) === $filename) {
            $baseUrl = $this->server->get('ORIG_SCRIPT_NAME'); // 1and1 shared hosting compatibility
        } else {
            // Backtrack up the script_filename to find the portion matching
            // php_self
            $path    = $this->server->get('PHP_SELF', '');
            $file    = $this->server->get('SCRIPT_FILENAME', '');
            $segs    = explode('/', trim($file, '/'));
            $segs    = array_reverse($segs);
            $index   = 0;
            $last    = count($segs);
            $baseUrl = '';
            do {
                $seg     = $segs[$index];
                $baseUrl = '/' . $seg . $baseUrl;
                ++$index;
            } while ($last > $index && (false !== $pos = strpos($path, $baseUrl)) && 0 != $pos);
        }
        
        // Does the baseUrl have anything in common with the request_uri?
        $requestUri = $this->getRequestUri();
        
        if ($baseUrl && false !== $prefix = $this->getUrlencodedPrefix($requestUri, $baseUrl)) {
            // full $baseUrl matches
            return $prefix;
        }
        
        if ($baseUrl && false !== $prefix = $this->getUrlencodedPrefix($requestUri, dirname($baseUrl))) {
            // directory portion of $baseUrl matches
            return rtrim($prefix, '/');
        }
        
        $truncatedRequestUri = $requestUri;
        if (false !== $pos = strpos($requestUri, '?')) {
            $truncatedRequestUri = substr($requestUri, 0, $pos);
        }
        
        $basename = basename($baseUrl);
        if (empty($basename) || !strpos(rawurldecode($truncatedRequestUri), $basename)) {
            // no match whatsoever; set it blank
            return '';
        }
        
        // If using mod_rewrite or ISAPI_Rewrite strip the script filename
        // out of baseUrl. $pos !== 0 makes sure it is not matching a value
        // from PATH_INFO or QUERY_STRING
        if (strlen($requestUri) >= strlen($baseUrl) && (false !== $pos = strpos($requestUri, $baseUrl)) && $pos !== 0) {
            $baseUrl = substr($requestUri, 0, $pos + strlen($baseUrl));
        }
        
        return rtrim($baseUrl, '/');
    }
    
    protected function prepareRequestUri()
    {
        $requestUri = '';
        
        if ($this->headers->has('X_ORIGINAL_URL')) {
            // IIS with Microsoft Rewrite Module
            $requestUri = $this->headers->get('X_ORIGINAL_URL');
            $this->headers->remove('X_ORIGINAL_URL');
            $this->server->remove('HTTP_X_ORIGINAL_URL');
            $this->server->remove('UNENCODED_URL');
            $this->server->remove('IIS_WasUrlRewritten');
        } elseif ($this->headers->has('X_REWRITE_URL')) {
            // IIS with ISAPI_Rewrite
            $requestUri = $this->headers->get('X_REWRITE_URL');
            $this->headers->remove('X_REWRITE_URL');
        } elseif ($this->server->get('IIS_WasUrlRewritten') == '1' && $this->server->get('UNENCODED_URL') != '') {
            // IIS7 with URL Rewrite: make sure we get the unencoded URL (double slash problem)
            $requestUri = $this->server->get('UNENCODED_URL');
            $this->server->remove('UNENCODED_URL');
            $this->server->remove('IIS_WasUrlRewritten');
        } elseif ($this->server->has('REQUEST_URI')) {
            $requestUri = $this->server->get('REQUEST_URI');
            // HTTP proxy reqs setup request URI with scheme and host [and port] + the URL path, only use URL path
            $schemeAndHttpHost = $this->getSchemeAndHttpHost();
            if (strpos($requestUri, $schemeAndHttpHost) === 0) {
                $requestUri = substr($requestUri, strlen($schemeAndHttpHost));
            }
        } elseif ($this->server->has('ORIG_PATH_INFO')) {
            // IIS 5.0, PHP as CGI
            $requestUri = $this->server->get('ORIG_PATH_INFO');
            if ('' != $this->server->get('QUERY_STRING')) {
                $requestUri .= '?' . $this->server->get('QUERY_STRING');
            }
            $this->server->remove('ORIG_PATH_INFO');
        }
        
        // normalize the request URI to ease creating sub-requests from this request
        $this->server->set('REQUEST_URI', $requestUri);
        
        return $requestUri;
    }
    
    protected function prepareBasePath()
    {
        $filename = basename($this->server->get('SCRIPT_FILENAME'));
        $baseUrl  = $this->getBaseUrl();
        if (empty($baseUrl)) {
            return '';
        }
        
        if (basename($baseUrl) === $filename) {
            $basePath = dirname($baseUrl);
        } else {
            $basePath = $baseUrl;
        }
        
        if ('\\' === DIRECTORY_SEPARATOR) {
            $basePath = str_replace('\\', '/', $basePath);
        }
        
        return rtrim($basePath, '/');
    }
    
    private function getUrlencodedPrefix($string, $prefix)
    {
        if (0 !== strpos(rawurldecode($string), $prefix)) {
            return false;
        }
        
        $len = strlen($prefix);
        
        if (preg_match("#^(%[[:xdigit:]]{2}|.){{$len}}#", $string, $match)) {
            return $match[0];
        }
        
        return false;
    }
    
    public function normalizeQueryString($qs)
    {
        if ('' == $qs) {
            return '';
        }
        
        $parts = array();
        $order = array();
        
        foreach (explode('&', $qs) as $param) {
            if ('' === $param || '=' === $param[0]) {
                // Ignore useless delimiters, e.g. "x=y&".
                // Also ignore pairs with empty key, even if there was a value, e.g. "=value", as such nameless values cannot be retrieved anyway.
                // PHP also does not include them when building _GET.
                continue;
            }
            
            $keyValuePair = explode('=', $param, 2);
            
            // GET parameters, that are submitted from a HTML form, encode spaces as "+" by default (as defined in enctype application/x-www-form-urlencoded).
            // PHP also converts "+" to spaces when filling the global _GET or when using the function parse_str. This is why we use urldecode and then normalize to
            // RFC 3986 with rawurlencode.
            $parts[] = isset($keyValuePair[1])
                ?
                rawurlencode(urldecode($keyValuePair[0])) . '=' . rawurlencode(urldecode($keyValuePair[1]))
                :
                rawurlencode(urldecode($keyValuePair[0]));
            $order[] = urldecode($keyValuePair[0]);
        }
        
        array_multisort($order, SORT_ASC, $parts);
        
        return implode('&', $parts);
    }
}

class Seowork_ParameterBag implements IteratorAggregate, Countable
{
    /**
     * Parameter storage.
     *
     * @var array
     */
    protected $parameters;
    
    /**
     * Constructor.
     *
     * @param array $parameters An array of parameters
     *
     * @api
     */
    public function __construct(array $parameters = array())
    {
        $this->parameters = $parameters;
    }
    
    /**
     * Returns the parameters.
     *
     * @return array An array of parameters
     *
     * @api
     */
    public function all()
    {
        return $this->parameters;
    }
    
    /**
     * Returns the parameter keys.
     *
     * @return array An array of parameter keys
     *
     * @api
     */
    public function keys()
    {
        return array_keys($this->parameters);
    }
    
    /**
     * Replaces the current parameters by a new set.
     *
     * @param array $parameters An array of parameters
     *
     * @api
     */
    public function replace(array $parameters = array())
    {
        $this->parameters = $parameters;
    }
    
    /**
     * Adds parameters.
     *
     * @param array $parameters An array of parameters
     *
     * @api
     */
    public function add(array $parameters = array())
    {
        if (sizeof($parameters)) {
            foreach ($parameters as $key => $value) {
                $this->parameters[$key] = $value;
            }
        }
    }
    
    /**
     * Returns a parameter by name.
     *
     * @param string $path    The key
     * @param mixed  $default The default value if the parameter key does not exist
     * @param bool   $deep    If true, a path like foo[bar] will find deeper items
     *
     * @return mixed
     *
     * @throws \InvalidArgumentException
     *
     * @api
     */
    public function get($path, $default = null, $deep = false)
    {
        if (!$deep || false === $pos = strpos($path, '[')) {
            return array_key_exists($path, $this->parameters) ? $this->parameters[$path] : $default;
        }
        
        $root = substr($path, 0, $pos);
        if (!array_key_exists($root, $this->parameters)) {
            return $default;
        }
        
        $value      = $this->parameters[$root];
        $currentKey = null;
        for ($i = $pos, $c = strlen($path); $i < $c; $i++) {
            $char = $path[$i];
            
            if ('[' === $char) {
                if (null !== $currentKey) {
                    throw new InvalidArgumentException(sprintf('Malformed path. Unexpected "[" at position %d.', $i));
                }
                
                $currentKey = '';
            } elseif (']' === $char) {
                if (null === $currentKey) {
                    throw new InvalidArgumentException(sprintf('Malformed path. Unexpected "]" at position %d.', $i));
                }
                
                if (!is_array($value) || !array_key_exists($currentKey, $value)) {
                    return $default;
                }
                
                $value      = $value[$currentKey];
                $currentKey = null;
            } else {
                if (null === $currentKey) {
                    throw new InvalidArgumentException(
                        sprintf('Malformed path. Unexpected "%s" at position %d.', $char, $i)
                    );
                }
                
                $currentKey .= $char;
            }
        }
        
        if (null !== $currentKey) {
            throw new InvalidArgumentException(sprintf('Malformed path. Path must end with "]".'));
        }
        
        return $value;
    }
    
    /**
     * Sets a parameter by name.
     *
     * @param string $key   The key
     * @param mixed  $value The value
     *
     * @api
     */
    public function set($key, $value)
    {
        $this->parameters[$key] = $value;
    }
    
    /**
     * Returns true if the parameter is defined.
     *
     * @param string $key The key
     *
     * @return bool    true if the parameter exists, false otherwise
     *
     * @api
     */
    public function has($key)
    {
        return array_key_exists($key, $this->parameters);
    }
    
    /**
     * Removes a parameter.
     *
     * @param string $key The key
     *
     * @api
     */
    public function remove($key)
    {
        unset($this->parameters[$key]);
    }
    
    /**
     * Returns the alphabetic characters of the parameter value.
     *
     * @param string $key     The parameter key
     * @param mixed  $default The default value if the parameter key does not exist
     * @param bool   $deep    If true, a path like foo[bar] will find deeper items
     *
     * @return string The filtered value
     *
     * @api
     */
    public function getAlpha($key, $default = '', $deep = false)
    {
        return preg_replace('/[^[:alpha:]]/', '', $this->get($key, $default, $deep));
    }
    
    /**
     * Returns the alphabetic characters and digits of the parameter value.
     *
     * @param string $key     The parameter key
     * @param mixed  $default The default value if the parameter key does not exist
     * @param bool   $deep    If true, a path like foo[bar] will find deeper items
     *
     * @return string The filtered value
     *
     * @api
     */
    public function getAlnum($key, $default = '', $deep = false)
    {
        return preg_replace('/[^[:alnum:]]/', '', $this->get($key, $default, $deep));
    }
    
    /**
     * Returns the digits of the parameter value.
     *
     * @param string $key     The parameter key
     * @param mixed  $default The default value if the parameter key does not exist
     * @param bool   $deep    If true, a path like foo[bar] will find deeper items
     *
     * @return string The filtered value
     *
     * @api
     */
    public function getDigits($key, $default = '', $deep = false)
    {
        // we need to remove - and + because they're allowed in the filter
        return str_replace(
            array(
                '-',
                '+',
            ),
            '',
            $this->filter($key, $default, $deep, FILTER_SANITIZE_NUMBER_INT)
        );
    }
    
    /**
     * Returns the parameter value converted to integer.
     *
     * @param string $key     The parameter key
     * @param mixed  $default The default value if the parameter key does not exist
     * @param bool   $deep    If true, a path like foo[bar] will find deeper items
     *
     * @return int     The filtered value
     *
     * @api
     */
    public function getInt($key, $default = 0, $deep = false)
    {
        return (int) $this->get($key, $default, $deep);
    }
    
    /**
     * Filter key.
     *
     * @param string $key     Key.
     * @param mixed  $default Default = null.
     * @param bool   $deep    Default = false.
     * @param int    $filter  FILTER_* constant.
     * @param mixed  $options Filter options.
     *
     * @see http://php.net/manual/en/function.filter-var.php
     *
     * @return mixed
     */
    public function filter($key, $default = null, $deep = false, $filter = FILTER_DEFAULT, $options = array())
    {
        $value = $this->get($key, $default, $deep);
        
        // Always turn $options into an array - this allows filter_var option shortcuts.
        if (!is_array($options) && $options) {
            $options = array('flags' => $options);
        }
        
        // Add a convenience check for arrays.
        if (is_array($value) && !isset($options['flags'])) {
            $options['flags'] = FILTER_REQUIRE_ARRAY;
        }
        
        return filter_var($value, $filter, $options);
    }
    
    /**
     * Returns an iterator for parameters.
     *
     * @return \ArrayIterator An \ArrayIterator instance
     */
    public function getIterator()
    {
        return new ArrayIterator($this->parameters);
    }
    
    /**
     * Returns the number of parameters.
     *
     * @return int The number of parameters
     */
    public function count()
    {
        return count($this->parameters);
    }
}

class Seowork_Http_ServerBag extends Seowork_ParameterBag
{
    /**
     * Gets the HTTP headers.
     *
     * @return array
     */
    public function getHeaders()
    {
        $headers        = array();
        $contentHeaders = array(
            'CONTENT_LENGTH' => true,
            'CONTENT_MD5'    => true,
            'CONTENT_TYPE'   => true,
        );
        foreach ($this->parameters as $key => $value) {
            if (0 === strpos($key, 'HTTP_')) {
                $headers[substr($key, 5)] = $value;
            } // CONTENT_* are not prefixed with HTTP_
            elseif (isset($contentHeaders[$key])) {
                $headers[$key] = $value;
            }
        }
        
        if (isset($this->parameters['PHP_AUTH_USER'])) {
            $headers['PHP_AUTH_USER'] = $this->parameters['PHP_AUTH_USER'];
            $headers['PHP_AUTH_PW']   = isset($this->parameters['PHP_AUTH_PW']) ? $this->parameters['PHP_AUTH_PW'] : '';
        } else {
            /*
             * php-cgi under Apache does not pass HTTP Basic user/pass to PHP by default
             * For this workaround to work, add these lines to your .htaccess file:
             * RewriteCond %{HTTP:Authorization} ^(.+)$
             * RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
             *
             * A sample .htaccess file:
             * RewriteEngine On
             * RewriteCond %{HTTP:Authorization} ^(.+)$
             * RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
             * RewriteCond %{REQUEST_FILENAME} !-f
             * RewriteRule ^(.*)$ app.php [QSA,L]
             */
            
            $authorizationHeader = null;
            if (isset($this->parameters['HTTP_AUTHORIZATION'])) {
                $authorizationHeader = $this->parameters['HTTP_AUTHORIZATION'];
            } elseif (isset($this->parameters['REDIRECT_HTTP_AUTHORIZATION'])) {
                $authorizationHeader = $this->parameters['REDIRECT_HTTP_AUTHORIZATION'];
            }
            
            if (null !== $authorizationHeader) {
                if (0 === stripos($authorizationHeader, 'basic')) {
                    // Decode AUTHORIZATION header into PHP_AUTH_USER and PHP_AUTH_PW when authorization header is basic
                    $exploded = explode(':', base64_decode(substr($authorizationHeader, 6)));
                    if (count($exploded) == 2) {
                        list($headers['PHP_AUTH_USER'], $headers['PHP_AUTH_PW']) = $exploded;
                    }
                } elseif (empty($this->parameters['PHP_AUTH_DIGEST']) && (0 === stripos(
                            $authorizationHeader,
                            'digest'
                        ))) {
                    // In some circumstances PHP_AUTH_DIGEST needs to be set
                    $headers['PHP_AUTH_DIGEST']          = $authorizationHeader;
                    $this->parameters['PHP_AUTH_DIGEST'] = $authorizationHeader;
                }
            }
        }
        
        // PHP_AUTH_USER/PHP_AUTH_PW
        if (isset($headers['PHP_AUTH_USER'])) {
            $headers['AUTHORIZATION'] = 'Basic ' . base64_encode(
                    $headers['PHP_AUTH_USER'] . ':' . $headers['PHP_AUTH_PW']
                );
        } elseif (isset($headers['PHP_AUTH_DIGEST'])) {
            $headers['AUTHORIZATION'] = $headers['PHP_AUTH_DIGEST'];
        }
        
        return $headers;
    }
}

class Seowork_Storage_PDO_SQLite
{
    private $md5;
    private $url;
    private $error;
    /**
     * @var \PDO
     */
    protected $driver;
    protected $status = false;
    protected $count  = 0;
    protected $log    = array();
    
    public function __construct($md5, $url, $dsn, array $options)
    {
        $this->md5 = $md5;
        $this->url = $url;
        
        try {
            $this->driver = new PDO($dsn);
            $this->status = true;
            
            $this->driver->exec('PRAGMA encoding = "UTF-8";');
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
        }
    }
    
    /**
     * @param $attr
     *
     * @return mixed
     */
    public function getAttribute($attr)
    {
        return $this->driver->getAttribute($attr);
    }
    
    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }
    
    /**
     * @return array
     */
    public static function getAvailableDrivers()
    {
        return PDO::getAvailableDrivers();
    }
    
    /**
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * @return \PDO
     */
    public function getDriver()
    {
        return $this->driver;
    }
    
    /**
     * @param $hash
     *
     * @return null|array
     */
    public function findAllByHash($hash)
    {
        $query  = $this->driver->query(sprintf("SELECT id, anchor FROM Web_IT_Url WHERE md5 = '%s'", $hash));
        $result = $query->fetchAll();
        
        if (empty($result)) {
            return null;
        }
        
        $this->count = count($result);
        
        return array_column($result, 'anchor', 'id');
    }
    
    /**
     * @param array $array
     *
     * @return bool
     */
    public function delete(array $array = array())
    {
        if ($size = count($array)) {
            $query = 'DELETE FROM Web_IT_Url WHERE id';
            
            $query .= ' IN (';
            
            $tmp  = array();
            $bind = array();
            
            foreach ($array as $id => $md5) {
                $k = ':id' . $id;
                
                $bind[$k] = (int) $id;
                
                $tmp[] = $k;
            }
            
            $query .= implode(',', $tmp);
            
            $query .= ')';
            
            $this->driver->beginTransaction();
            
            $query = $this->driver->prepare($query);
            
            try {
                $query->execute($bind);
                
                $this->driver->commit();
                
                return true;
            } catch (PDOException $e) {
                $this->error = $e->getMessage();
                Seowork_Debugger::debug($e);
            }
        }
        
        return false;
    }
    
    /**
     * @param array $array
     *
     * @return array|bool
     */
    public function save(array $array = array())
    {
        if (sizeof($array)) {
            $saved = array();
            
            $this->driver->beginTransaction();
            
            $queryInsert = $this->driver->prepare(
                'INSERT INTO Web_IT_Url (id, md5, url, anchor, cdate) VALUES (:id, :md5, :url, :anchor, :cdate);'
            );
            $queryUpdate = $this->driver->prepare(
                'UPDATE Web_IT_Url SET md5 = :md5, url = :url, cdate = :cdate WHERE id = :id;'
            );
            
            try {
                foreach ($array as $id => $anchor) {
                    try {
                        if ($result = $queryInsert->execute(
                            array(
                                ':id'     => (int) $id,
                                ':md5'    => $this->md5,
                                ':url'    => $this->url,
                                ':anchor' => $anchor,
                                ':cdate'  => (string) date('Y-m-d H:i:s'),
                            )
                        )) {
                            $saved[$id] = $anchor;
                        }
                        
                        $this->log[] = array(
                            'query'  => $queryInsert->queryString,
                            'params' => array(
                                ':id'     => (int) $id,
                                ':md5'    => $this->md5,
                                ':url'    => $this->url,
                                ':anchor' => $anchor,
                            ),
                            'result' => $result,
                            'error'  => array(
                                'code' => $queryInsert->errorCode(),
                                'info' => $queryInsert->errorInfo(),
                            ),
                        );
                    } catch (PDOException $e) {
                        if ($e->getCode() != 2300) {
                            throw $e;
                        }
                    }
                }
                
                $this->driver->commit();
                
                return $saved;
            } catch (PDOException $e) {
                $this->error = $e->getMessage();
                Seowork_Debugger::debug($e);
            }
        }
        
        return false;
    }
}

class Seowork_Storage_PDO_MySQL
{
    protected $md5;
    protected $url;
    protected $error;
    protected $table;
    /**
     * @var \PDO
     */
    protected $driver;
    protected $status = false;
    protected $count  = 0;
    protected $log    = array();
    
    /**
     * Seowork_Storage_PDO constructor.
     *
     * @param string $table
     * @param string $md5
     * @param string $url
     * @param string $dsn
     * @param string $user
     * @param string $password
     * @param array  $options
     */
    public function __construct($table, $md5, $url, $dsn, $user, $password, array $options)
    {
        $this->md5   = $md5;
        $this->url   = $url;
        $this->table = $table;
        
        $options = array_merge(
            $options,
            array(
                PDO::ATTR_PERSISTENT => true,
                PDO::ATTR_ERRMODE    => PDO::ERRMODE_EXCEPTION,
            )
        );
        
        try {
            $this->driver = new PDO($dsn, $user, $password, $options);
            $this->status = true;
            
            $this->driver->exec(
                "CREATE TABLE IF NOT EXISTS `{$this->table}` (
                    `id` int(11) unsigned NOT NULL,
                    `md5` varchar(32) CHARACTER SET cp1251 NOT NULL,
                    `url` text CHARACTER SET cp1251 NOT NULL,
                    `anchor` text CHARACTER SET cp1251 NOT NULL,
                    `cdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`id`),
                    KEY `md5` (`md5`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            "
            );
            
            $this->driver->exec('SET NAMES UTF8');
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
        }
    }
    
    /**
     * @param $attr
     *
     * @return mixed
     */
    public function getAttribute($attr)
    {
        return $this->driver->getAttribute($attr);
    }
    
    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }
    
    /**
     * @return array
     */
    public static function getAvailableDrivers()
    {
        return PDO::getAvailableDrivers();
    }
    
    /**
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * @return \PDO
     */
    public function getDriver()
    {
        return $this->driver;
    }
    
    /**
     * @param $hash
     *
     * @return null|array
     */
    public function findAllByHash($hash)
    {
        $query = "SELECT id, anchor FROM {$this->table} WHERE md5 = :hash";
        $query = $this->driver->prepare($query);
        $query->execute(array(':hash' => $hash));
        
        $result = null;
        
        if ($this->count = $query->rowCount()) {
            while ($row = $query->fetch(PDO::FETCH_OBJ)) {
                $result[(int) $row->id] = $row->anchor;
            }
        }
        
        return $result;
    }
    
    /**
     * @param array $array
     *
     * @return array|bool
     */
    public function save(array $array = array())
    {
        if (sizeof($array)) {
            $saved = array();
            
            $this->driver->beginTransaction();
            
            $query = "INSERT INTO {$this->table} (id, md5, url, anchor) VALUES (:id, :md5, :url, :anchor) ON DUPLICATE KEY UPDATE md5 = :md5, url = :url";
            $query = $this->driver->prepare($query);
            
            try {
                foreach ($array as $id => $anchor) {
                    if ($result = $query->execute(
                        array(
                            ':id'     => (int) $id,
                            ':md5'    => $this->md5,
                            ':url'    => $this->url,
                            ':anchor' => $anchor,
                        )
                    )) {
                        $saved[$id] = $anchor;
                    }
                    $this->log[] = array(
                        'query'  => $query->queryString,
                        'params' => array(
                            ':id'     => (int) $id,
                            ':md5'    => $this->md5,
                            ':url'    => $this->url,
                            ':anchor' => $anchor,
                        ),
                        'result' => $result,
                        'error'  => array(
                            'code' => $query->errorCode(),
                            'info' => $query->errorInfo(),
                        ),
                    );
                }
                
                $this->driver->commit();
                
                return $saved;
            } catch (PDOException $e) {
                $this->error = $e->getMessage();
                Seowork_Debugger::debug($e);
            }
        }
        
        return false;
    }
    
    /**
     * @param array $array
     *
     * @return bool
     */
    public function delete(array $array = array())
    {
        if ($size = sizeof($array)) {
            $query = "DELETE FROM {$this->table} WHERE id";
            
            $query .= ' IN (';
            
            $tmp  = array();
            $bind = array();
            
            foreach ($array as $id => $md5) {
                $k = ':id' . $id;
                
                $bind[$k] = (int) $id;
                
                $tmp[] = $k;
            }
            
            $query .= join(',', $tmp);
            
            $query .= ')';
            
            $this->driver->beginTransaction();
            
            $query = $this->driver->prepare($query);
            
            try {
                $query->execute($bind);
                
                $this->driver->commit();
                
                return true;
            } catch (PDOException $e) {
                $this->error = $e->getMessage();
                Seowork_Debugger::debug($e);
            }
        }
        
        return false;
    }
}

class Seowork_Debugger
{
    const DEBUG_HEADER = 'X-4SEO-DEBUG';
    static $debugModeOn = false;
    
    public static function debug()
    {
        if (Seowork_Debugger::$debugModeOn) {
            Seowork_Debugger::debugMessage(func_get_args());
        }
    }
    
    protected static function debugMessage($args)
    {
        $backtrace = debug_backtrace();
        echo sprintf(
            '</br> In class <b>%s</b>, method <b>%s</b>. On line <b>%d</b>. ',
            $backtrace[2]['class'],
            $backtrace[2]['function'],
            $backtrace[1]['line']
        );
        if (!empty($args)) {
            echo '<pre>';
            foreach ($args as $arg) {
                var_dump($arg);
            }
            echo '</pre>';
        }
        echo str_repeat('-', 100);
    }
    
    public static function enableDebugMode()
    {
        Seowork_Debugger::$debugModeOn = true;
    }
    
    public static function disableDebugMode()
    {
        Seowork_Debugger::$debugModeOn = false;
    }
}
